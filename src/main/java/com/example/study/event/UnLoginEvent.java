package com.example.study.event;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;

public class UnLoginEvent extends ApplicationEvent {
    public UnLoginEvent(ApplicationContext source){
        super(source);
    }
}
