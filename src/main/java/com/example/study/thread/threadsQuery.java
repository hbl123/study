package com.example.study.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class threadsQuery {

    /**
     * 多线程查询
     * @param args
     */
    public static void main(String[] args) {
        ExecutorService executors = Executors.newFixedThreadPool(10);
        List<CompletableFuture<Integer>> comparableList = new ArrayList<>();
        List<Integer> result = new ArrayList<>();
        for(int i=0;i<100;i++){
            int a =i;
            comparableList.add(CompletableFuture.supplyAsync(()->doSleepReturn(a),executors));
        }
        CompletableFuture allof = CompletableFuture.allOf(comparableList.toArray(new CompletableFuture[comparableList.size()]))
                .whenComplete((v,th)-> comparableList.forEach(s->{
                    try {
                        result.add(s.get());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }) );
        allof.join();
        System.out.println(result);
    }

    public static Integer doSleepReturn(int a){
        int random =(int)(1+Math.random()*1000);
        try {
            Thread.sleep(random);
            log.info("当前线程{}，a:{},sleep了{}ms;",Thread.currentThread().getId(),a,random);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return a;
    }

}
