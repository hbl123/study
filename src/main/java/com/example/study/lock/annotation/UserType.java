package com.example.study.lock.annotation;


import java.lang.annotation.*;

//只对方法生效
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UserType {
    /**
     * 默认的用户类型
     */
    String[] type() default {};
}
