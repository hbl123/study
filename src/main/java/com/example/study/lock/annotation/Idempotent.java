package com.example.study.lock.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Idempotent {
    /**
     * key
     * @return
     */
    String key();

    /**
     * 前缀
     * @return
     */
    String prefix();

    /**
     * 过期时间秒
     * @return
     */
    long expireTime() default 50L;

    /**
     * 提示信息
     * @return
     */
    String info() default "请勿重复提交";

    int retryTimes() default  Integer.MAX_VALUE;
}
