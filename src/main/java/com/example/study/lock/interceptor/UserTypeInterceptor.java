package com.example.study.lock.interceptor;

import com.example.study.lock.annotation.UserType;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class UserTypeInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception{
        if(handler instanceof HandlerMethod){
            //判断是否存在注解
            HandlerMethod handlerMethod =(HandlerMethod) handler;
            //判断是否存在UserType注解
            boolean hasUSerTypeAnnotation = handlerMethod.getMethod().isAnnotationPresent(UserType.class);
            if(hasUSerTypeAnnotation){
                //查看注解的值
                UserType serializedField = handlerMethod.getMethodAnnotation(UserType.class);

            }
        }
        return super.preHandle(request,response,handler);
    }
}
