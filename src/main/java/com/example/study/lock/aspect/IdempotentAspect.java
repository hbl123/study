package com.example.study.lock.aspect;

import com.example.study.lock.RedisDistributedLock;
import com.example.study.lock.annotation.Idempotent;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


import java.lang.reflect.Method;

@Aspect
@Slf4j
@Component
public class IdempotentAspect {
    //定义切点Pointcut

    @Autowired
    private RedisDistributedLock redisDistributedLock;
    @Around("@annotation(com.example.study.lock.annotation.Idempotent)")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable{
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method  method = signature.getMethod();
        Idempotent idempotent = method.getAnnotation(Idempotent.class);
        String key = this.getLockKey(method,pjp,idempotent);
        log.info("{} start lock" , key);
        if(!redisDistributedLock.lock(key,idempotent.expireTime(),idempotent.retryTimes())){
            log.error("{} lock failed" ,key);
            throw new RuntimeException(idempotent.info());
        }
        try{
            log.info("{} get lock" ,key);
            Object result = pjp.proceed();
            return result;
        }finally {
            log.info("{} end lock ",key);
            redisDistributedLock.releaseLock(key);
        }
    }

    private String getLockKey(Method targetMethod,ProceedingJoinPoint joinPoint,Idempotent targetAnnotation)
            throws RuntimeException{
        Object target = joinPoint.getTarget();
        Object[] arguments = joinPoint.getArgs();
        String body = parse(target ,targetAnnotation.key(),targetMethod,arguments);
        if(StringUtils.isEmpty(body)){
            throw new RuntimeException("业务主键不能为空");
        }
        return targetAnnotation.prefix()+body;
    }

    public static String parse(Object rootObject,String key,Method method, Object[] args){
        LocalVariableTableParameterNameDiscoverer u = new LocalVariableTableParameterNameDiscoverer();
        String[] paraNameArr = u.getParameterNames(method);
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new MethodBasedEvaluationContext(rootObject,method,args,u);
        if(paraNameArr!=null){
            for(int i=0;i<paraNameArr.length;i++){
                context.setVariable(paraNameArr[i],args[i]);
            }
        }
        return  parser.parseExpression(key).getValue(context,String.class);
    }


}
