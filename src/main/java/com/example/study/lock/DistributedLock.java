package com.example.study.lock;

public interface DistributedLock {

    public static final long TIMEOUT_MILLIS = 20000;
    public static final int RETRY_TIMES = Integer.MAX_VALUE;
    public static final long SlEEP_MILLIS = 500;

    public boolean lock(String key);
    public boolean lock(String key,int retryTimes);
    public boolean lock(String key,int retryTimes,long sleepMillis);
    public boolean lock(String key,long expire);
    public boolean lock(String key,long expire,int retryTimes);
    public boolean lock(String key,long expire,int retryTimes,long sleepMillis);
    public boolean releaseLock(String key);

}
