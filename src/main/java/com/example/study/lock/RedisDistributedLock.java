package com.example.study.lock;

import com.example.study.cache.redis.RedisCacheFactory;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.ScriptOutputType;
import io.lettuce.core.SetArgs;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.api.async.RedisScriptingAsyncCommands;
import io.lettuce.core.cluster.api.async.RedisAdvancedClusterAsyncCommands;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Slf4j
@Component
public class RedisDistributedLock extends AbstractDistributedLock {

    @Autowired
    RedisCacheFactory redisCacheFactory;
    @Autowired
    LettuceConnectionFactory lettuceConnectionFactory;
    private RedisTemplate<String, Object> redisTemplate;

    private ThreadLocal<String> lockFlag = new ThreadLocal<String>();

    public static final String UNLOCK_LUA;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append("if redis.call(\"get\",KEYS[1]) == ARGV[1] ");
        sb.append("then ");
        sb.append("    return redis.call(\"del\",KEYS[1]) ");
        sb.append("else ");
        sb.append("    return 0 ");
        sb.append("end ");
        UNLOCK_LUA = sb.toString();
    }

    public RedisTemplate<String, Object> createRedisTemplate() {
        if(this.redisTemplate == null) {
            this.redisTemplate = redisCacheFactory.createRedisTemplate(lettuceConnectionFactory,"lock");
        }
        return redisTemplate;
    }

    @Override
    public boolean lock(String key,long expire , int retryTimes,long sleepMillis){
        boolean result = setRedis(key,expire);
        //如果获取锁失败，按照传入的重试次数进行重试
        while((!result)&&retryTimes-->0){
            try{
                log.debug("lock failed,retrying..."+retryTimes);
                Thread.sleep(sleepMillis);
            }catch (InterruptedException e){
                return false;
            }
            result = setRedis(key,expire);
        }
        return result;
    }

    private boolean setRedis (String key,long expire){
        String uuid = UUID.randomUUID().toString();
        byte[] uuidBytes = uuid.getBytes(StandardCharsets.UTF_8);
        try{
            String result = createRedisTemplate().execute(new RedisCallback<String>() {
                @Override
                public String doInRedis(RedisConnection connection) throws DataAccessException {
                    byte[] redisKey = key.getBytes(StandardCharsets.UTF_8);
                    String result = "" ;
                    Object nativeConnection = connection.getNativeConnection();
                    if(nativeConnection instanceof RedisAdvancedClusterAsyncCommands){
                        RedisAdvancedClusterAsyncCommands command
                                =(RedisAdvancedClusterAsyncCommands) nativeConnection;
                        result = getEvalResultString(command.getStatefulConnection().async().set(redisKey, uuidBytes,
                                SetArgs.Builder.nx().ex(expire)));
                    }else if(nativeConnection instanceof RedisAsyncCommands){
                        RedisAsyncCommands command = (RedisAsyncCommands) nativeConnection ;
                        result =  getEvalResultString(command.getStatefulConnection().async().set(redisKey, uuidBytes,
                                SetArgs.Builder.nx().ex(expire)));
                    }
                    return result;
                }
            });
            if("OK".equals(result)){
                lockFlag.set(uuid);
                return true;
            }
            return false;
        }catch (Exception e){
            log.error("set redis occured an exception",e );
        }
        return false;
    }

    @Override
    public boolean releaseLock(String key){
        byte[] redisKey = key.getBytes(StandardCharsets.UTF_8);
        //释放锁的时候，有可能因为持锁之后方法执行时间大于锁的有效期，此时有可能已经被另外一个线程持有锁，所以不能直接删除
        try{
            byte[] args = lockFlag.get().getBytes(StandardCharsets.UTF_8);
            Object[] keyParam = new Object[]{redisKey};
            Long result = createRedisTemplate().execute(new RedisCallback<Long>() {
                @Override
                public Long doInRedis(RedisConnection connection) throws DataAccessException {
                    Object nativeConnection = connection.getNativeConnection();
                    //集群模式和单机模式虽然执行脚本的方法一样，但是没有共同的接口，所以只能分开执行
                    //集群模式
                    if(nativeConnection instanceof RedisScriptingAsyncCommands){
                        RedisScriptingAsyncCommands<Object,byte[]> commands
                                =(RedisScriptingAsyncCommands<Object,byte[]>)nativeConnection;
                        return getEvalResult(commands.eval(UNLOCK_LUA, ScriptOutputType.INTEGER,keyParam,args));
                    }else {
                        return 0L;
                    }

                }
            });
            return result!=null&&result>0;
        }catch (Exception e){
            log.error("release lock occured an exception",e);
        }
        return false;
    }

    private String getEvalResultString(RedisFuture future){
        try{
            Object o = future.get();
            return (String) o;
        }catch (Exception e){
            log.error("getEvalResultString occured an exception",e);
            return "0";
        }
    }

    private Long getEvalResult(RedisFuture future){
        try{
            Object o = future.get();
            return (Long) o;
        }catch (Exception e){
            log.error("getEvalResultString occured an exception",e);
            return 0L;
        }
    }

}
