package com.example.study.mybatis;


import com.alibaba.druid.DbType;
import com.alibaba.druid.sql.SQLUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;


import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

@Intercepts({
    @Signature(type = Executor.class,method = "update",args = {MappedStatement.class,Object.class}),
    @Signature(type = Executor.class,method = "query",args = {MappedStatement.class,Object.class,
            RowBounds.class, ResultHandler.class}),
    @Signature(type = Executor.class,method = "query",args = {MappedStatement.class,Object.class,
                RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class}),
})
@Slf4j
public class MybatisInterceptor implements Interceptor {

    private static final String SQL_ID_PREFIX = "com.xxx.infrastructure.mapper.";

    private SQLUtils.FormatOption statementSqlFormatOption = new SQLUtils.FormatOption(false,true);

    private Pattern patternBlank = Pattern.compile("\\s+");

    private String[] excludeSqlIds;

    public MybatisInterceptor(String[] excludeSqlIds){
        this.excludeSqlIds = excludeSqlIds;
    }

    private boolean isExcludeSql(String sqlId){
        for (String item : this.excludeSqlIds){
            if(sqlId.startsWith(item)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Object intercept(Invocation invocation)throws Throwable{
        Object[] args = invocation.getArgs();
        MappedStatement mappedStatement = (MappedStatement) args[0];
        String sqlId = mappedStatement.getId();
        if(this.isExcludeSql(sqlId)){
            return invocation.proceed();
        }

        int n = args.length;
        Object parameter = null;
        if(n>1){
            parameter = args[1];
        }
        BoundSql boundSql = mappedStatement.getBoundSql(parameter);
        Configuration configuration = mappedStatement.getConfiguration();
        long start = System.currentTimeMillis();
        Object ret = invocation.proceed();
        long timeCost = System.currentTimeMillis()-start;
        String sql = this.getSql(configuration,boundSql,sqlId,timeCost);
        log.info(sql);
        return ret;
    }

    private String getSql(Configuration configuration,BoundSql boundSql,String sqlId,long time){
        String formatSql = boundSql.getSql().replaceAll("[\\s]+"," ");
        List<Object> parameters = this.getParamList(configuration,boundSql);
        if(parameters.isEmpty()){
            formatSql = patternBlank.matcher(formatSql).replaceAll(" ");
        }else {
            String dbType = "oracle";
            formatSql = SQLUtils.format(formatSql, DbType.of(dbType),parameters,this.statementSqlFormatOption);
            formatSql = patternBlank.matcher(formatSql).replaceAll(" ");
        }

        StringBuilder str = new StringBuilder(512);
        String method = sqlId.substring(SQL_ID_PREFIX.length());
        str.append(method);
        str.append(": [");
        str.append(formatSql);
        str.append("] ");
        str.append(" ");
        str.append("time");
        str.append("ms");
        return str.toString();
    }

    private List<Object> getParamList(Configuration configuration,BoundSql boundSql){
        Object parameterObject = boundSql.getParameterObject();
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        List<Object> parameters = new ArrayList<>();
        MetaObject metaObject = configuration.newMetaObject(parameterObject);
        for (ParameterMapping parameterMapping: parameterMappings){
            String propertyName = parameterMapping.getProperty();
            Object obj;
            if(metaObject.hasGetter(propertyName)){
                obj = metaObject.getValue(propertyName);
                parameters.add(obj);
            }else if(boundSql.hasAdditionalParameter(propertyName)){
                obj = boundSql.getAdditionalParameter(propertyName);
                parameters.add(obj);
            }else {
                obj = metaObject.getOriginalObject();
                if(obj!=null){
                    parameters.add(obj);
                }
            }
        }
        return parameters;
    }

    String getParameterValue(Object obj){
        if(obj instanceof  String){
            return String.format("'%s'",obj);
        }else if(obj instanceof Date){
            DateFormat formatter = DateFormat.getDateTimeInstance(2,2, Locale.CHINA);
            return String.format("'%s'",formatter.format((Date)obj));
        }else if (obj!=null){
            return obj.toString();
        }
        return "";
    }

    //不用写默认这个
    @Override
    public Object plugin(Object target){
        return Plugin.wrap(target,this);
    }

}
