package com.example.study.mybatis;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisLogConfig {
    @Value("${mybatis.log.excludes}")
    private String[] excludeSqlIds;

    @Bean
    ConfigurationCustomizer mybatisConfigurationCustomizer(){
        return configuration ->configuration.addInterceptor(new MybatisInterceptor(excludeSqlIds));
    }
}
