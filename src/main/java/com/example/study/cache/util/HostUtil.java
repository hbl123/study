package com.example.study.cache.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.connection.RedisNode;


import java.util.ArrayList;
import java.util.List;

public class HostUtil {

    /**
     * 端口地址字符串转数组
     * @param addr
     * @return
     */
    public static List<RedisNode> parseRedisNodes(String addr){
        String[] hosts = addr.split(",");
        ArrayList<RedisNode> nodes = new ArrayList(hosts.length);
        String[] var3 =hosts;
        int var4 = hosts.length;
        for(int var5=0;var5<var4;++var5){
            String hostItem = var3[var5];
            if(StringUtils.isEmpty(hostItem)){
                String[] items = hostItem.split(":");
                String redisHost = items[0].trim();
                int port = items.length>1?Integer.parseInt(items[1]):6379;
                nodes.add(new RedisNode(redisHost,port));
            }
        }
        return nodes;
    }
}
