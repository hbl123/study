package com.example.study.cache.redis;

import org.quartz.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = {"ewb.cache.redis.heart"},matchIfMissing = false)
public class HealthCheckQuartzConfig {
    public HealthCheckQuartzConfig(){

    }

    @Bean
    public JobDetail printTimeJobDetail(){
        return JobBuilder.newJob(HealthCheckService.class).withIdentity("HealthCheckService").storeDurably().build();
    }

    @Bean
    public Trigger printTimeJobTrigger(){
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule("0/10 * * * * ?");
        return TriggerBuilder.newTrigger().forJob(this.printTimeJobDetail()).withIdentity("quartzTaskService").
                withSchedule(cronScheduleBuilder).build();
    }

}
