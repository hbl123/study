package com.example.study.cache.redis;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.nio.charset.StandardCharsets;

final class PrefixKeySerializer implements RedisSerializer<String> {
    private final String prefixkey;
    public PrefixKeySerializer(String prefixkey){
        this.prefixkey=prefixkey;
    }

    @Override
    public byte[] serialize(String key) throws SerializationException {
        return String.format("%s%s",this.prefixkey,key).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public String deserialize(byte[] bytes) throws SerializationException {
        return (new String(bytes, StandardCharsets.UTF_8)).substring(this.prefixkey.length());
    }
}
