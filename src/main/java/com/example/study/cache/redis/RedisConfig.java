package com.example.study.cache.redis;

import com.example.study.cache.util.HostUtil;
import io.lettuce.core.ClientOptions;
import io.lettuce.core.SocketOptions;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.util.CollectionUtils;


import java.time.Duration;
import java.util.HashSet;
import java.util.List;

public class RedisConfig {
    /**
     * redis地址
     */
    @Value("${efw.cache.redis.host:}")
    private String host;
    /**
     * 备用地址
     */
    @Value("${efw.cache.redis.hostBak:}")
    private String hostBak;
    /**
     * 当前使用地址
     */
    private String hostCurrent;
    /**
     * 连接超时
     */
    @Value("${study.cache.redis.connectionTimeout:5000}")
    private long connectTimeout;
    /**
     * 命令超时
     */
    @Value("${study.cache.redis.commandTimeout:10000}")
    private long commandTimeout;
    /**
     * 密码
     */
    @Value("${study.cache.redis.password:}")
    private String password;
    /**
     * maxTtl
     */
    @Value("${study.cache.redis.maxTtl:43200}")
    private int maxTtl;
    /**
     * maxTti
     */
    @Value("${study.cache.redis.maxTti:0}")
    private int maxTti;
    /**
     * maxTotal
     */
    @Value("${study.cache.redis.maxTotal:8}")
    private int maxTotal;
    /**
     * maxIdle
     */
    @Value("${study.cache.redis.maxIdle:8}")
    private int maxIdle;
    /**
     * maxWait
     */
    @Value("${study.cache.redis.maxWait:5000}")
    private int maxWait;
    /**
     * maxRedirects
     */
    @Value("${study.cache.redis.maxRedirects:3}")
    private int maxRedirects;
    /**
     * minIdle
     */
    @Value("${study.cache.redis.minIdle:1}")
    private int minIdle;
    static String formatFullKey(String prefixKey,String key){
        return String.format("%s%s:",prefixKey,key);
    }

    @Bean
    LettuceConnectionFactory lettuceConnectionFactory(){
        this.hostCurrent = this.host;
        return this.getConnectionFactory(this.host);
    }


    private LettuceConnectionFactory getConnectionFactory(String host){
        List<RedisNode> nodes = HostUtil.parseRedisNodes(host);
        if(CollectionUtils.isEmpty(nodes)){
            throw new RuntimeException("Redis未配置");
        }
        //this.password 解密重新赋值
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setMaxIdle(this.maxIdle);
        poolConfig.setMinIdle(this.minIdle);
        poolConfig.setMaxTotal(this.maxTotal);
        poolConfig.setMaxWaitMillis(this.maxWait);
        ClientOptions clientOptions = ClientOptions.builder().autoReconnect(true).
                socketOptions(SocketOptions.builder().connectTimeout(Duration.ofMillis(this.connectTimeout))
                        .keepAlive(true).tcpNoDelay(true).build()).build();
        LettucePoolingClientConfiguration lettucePoolingClientConfiguration = LettucePoolingClientConfiguration
                .builder().poolConfig(poolConfig).clientOptions(clientOptions)
                .commandTimeout(Duration.ofMillis(this.commandTimeout)).build();
        LettuceConnectionFactory connectionFactory;
        if(nodes.size()==1){
            RedisNode node = nodes.get(0);
            RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
            redisStandaloneConfiguration.setHostName(node.getHost());
            redisStandaloneConfiguration.setPort(node.getPort());
            if(StringUtils.isNoneBlank(this.password)){
                redisStandaloneConfiguration.setPassword(RedisPassword.of(this.password));
            }
            connectionFactory = new LettuceConnectionFactory(redisStandaloneConfiguration,
                    lettucePoolingClientConfiguration);
        }else {
            RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration();
            redisClusterConfiguration.setClusterNodes(new HashSet(nodes));
            redisClusterConfiguration.setMaxRedirects(this.maxRedirects);
            if(StringUtils.isNoneBlank(this.password)){
                redisClusterConfiguration.setPassword(RedisPassword.of(this.password));
            }
            connectionFactory = new LettuceConnectionFactory(redisClusterConfiguration,
                    lettucePoolingClientConfiguration);
        }
        connectionFactory.setShareNativeConnection(false);
        return connectionFactory;
    }
    
}
