package com.example.study.cache.redis;


import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

public class RedisCacheFactory {

    private final RedisConfig redisConfig;

    private final String prefixKey;

    private LettuceConnectionFactory lettuceConnectionFactory;

    public RedisCacheFactory(RedisConfig redisConfig,String prefixKey){
        this.redisConfig = redisConfig;
        this.lettuceConnectionFactory = redisConfig.lettuceConnectionFactory();
        this.prefixKey = prefixKey ;
    }
    
    public RedisTemplate<String,Object> createRedisTemplate(LettuceConnectionFactory lettuceConnectionFactory, String cacheName){
        //重写序列化方法
//        RedisSerializer valueSerializer = CustomValueSerializer.getInstance();
        RedisSerializer valueSerializer = new RedisSerializer() {
            public byte[] serialize(Object o) throws SerializationException {
                return new byte[0];
            }

            public Object deserialize(byte[] bytes) throws SerializationException {
                return null;
            }
        };
        return createRedisTemplate(lettuceConnectionFactory,cacheName,valueSerializer);
    }


    public RedisTemplate<String,Object> createRedisTemplate(LettuceConnectionFactory lettuceConnectionFactory,
                                                            String cacheName,RedisSerializer valueSerializer){
        RedisTemplate<String,Object> redisTemplate = new RedisTemplate<>();
        lettuceConnectionFactory.setShareNativeConnection(false);
        redisTemplate.setConnectionFactory(lettuceConnectionFactory);
        //设置序列化
        PrefixKeySerializer keySerializer = new PrefixKeySerializer
                (RedisConfig.formatFullKey(this.prefixKey,cacheName));
        redisTemplate.setKeySerializer(keySerializer);
        redisTemplate.setValueSerializer(valueSerializer);
        redisTemplate.setHashKeySerializer(keySerializer);
        redisTemplate.setHashValueSerializer(valueSerializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;

    }

}
